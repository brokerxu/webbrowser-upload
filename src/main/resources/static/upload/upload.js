(function ($) {
    // 当domReady的时候开始初始化
    $(function () {
        var $wrap = $('#uploader'),

            // 图片容器
            $queue = $('<ul class="filelist"></ul>')
                .appendTo($wrap.find('.queueList')),

            // 状态栏，包括进度和控制按钮
            $statusBar = $wrap.find('.statusBar'),

            // 文件总体选择信息。
            $info = $statusBar.find('.info'),

            // 上传按钮
            $upload = $wrap.find('.uploadBtn'),

            // 没选择文件之前的内容。
            $placeHolder = $wrap.find('.placeholder'),

            $progress = $statusBar.find('.progress').hide(),

            // 添加的文件数量
            fileCount = 0,

            // 添加的文件总大小
            fileSize = 0,

            // 优化retina, 在retina下这个值是2
            ratio = window.devicePixelRatio || 1,

            // 缩略图大小
            thumbnailWidth = 110 * ratio,
            thumbnailHeight = 110 * ratio,

            // 可能有pedding, ready, uploading, confirm, done.
            state = 'pedding',

            // 所有文件的进度信息，key为file id
            percentages = {},
            // 判断浏览器是否支持图片的base64
            isSupportBase64 = (function () {
                var data = new Image();
                var support = true;
                data.onload = data.onerror = function () {
                    if (this.width != 1 || this.height != 1) {
                        support = false;
                    }
                };
                data.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
                return support;
            })(),

            // 检测是否已经安装flash，检测flash的版本
            flashVersion = (function () {
                var version;

                try {
                    version = navigator.plugins['Shockwave Flash'];
                    version = version.description;
                } catch (ex) {
                    try {
                        version = new ActiveXObject('ShockwaveFlash.ShockwaveFlash')
                            .GetVariable('$version');
                    } catch (ex2) {
                        version = '0.0';
                    }
                }
                version = version.match(/\d+/g);
                return parseFloat(version[0] + '.' + version[1], 10);
            })(),

            supportTransition = (function () {
                var s = document.createElement('p').style,
                    r = 'transition' in s ||
                        'WebkitTransition' in s ||
                        'MozTransition' in s ||
                        'msTransition' in s ||
                        'OTransition' in s;
                s = null;
                return r;
            })(),

            // WebUploader实例
            uploader;
        /**
         * register一定要在Create之前，经过验证否则无效
         */
        WebUploader.Uploader.register({
            'init': 'init',
            "before-send-file": "beforeSendFile",
            "before-send": "beforeSend",
            "after-send-file": "afterSendFile"
        }, {
            init:function (opts) {
                console.log("加载中..."+opts)
                var str="";
                for (var item in opts){
                    str +=item+":"+opts[item]+"\n";
                }
                console.log(str);



            },
            /**
             * 在文件发送之前request，此时还没有分片（如果配置了分片的话），可以用来做文件整体md5验证。
             * @param file
             */
            beforeSendFile: function (file) {
                console.log("beforeSendFile");
                $upload.text('请等待');
                $upload.attr('disabled',true);
                var deferred = WebUploader.Base.Deferred();
                uploader.md5File(file, 0, 1024 * 1024).then(function (md5) {
                    file.md5 = md5;
                    var type = file.ext;
                    var size = file.size;
                    var chunksize = uploader.options.chunkSize;
                    var chunks = Math.floor(size/chunksize);
                    if(size % chunksize > 0){
                        chunks = chunks + 1;
                    }
                    console.log("计算的chunks:"+chunks);
                    /**
                     * 如果验证通过：
                     * 1：文件类型符合
                     * 2：文件已经存在，并下载完成
                     * 3: 文件存在，但未下载完成
                     */
                    $.ajax({
                        url: "../../../file/verify",
                        data: {md5: md5, type: type,chunks:chunks},
                        type: 'POST',
                        success: function (data) {
                            console.log(data);
                            var result = JSON.parse(data);
                            var code = result.code;
                            if ("0000" == code) {
                                //验证通过
                                deferred.resolve();
                            } else if("0001" == code){
                                //跳过此文件
                                deferred.resolve();
                                uploader.skipFile(file);
                            }else {
                                console.log(result.msg);
                                deferred.resolve();
                            }
                        },
                        error: function (req, err, ex) {
                            console.log(err);
                            //执行失败
                            deferred.fail();
                        }
                    })
                });
                return deferred.promise();
            },
            /**
             * 在分片发送之前request，可以用来做分片验证，如果此分片已经上传成功了，可返回一个rejected promise来跳过此分片上传
             * file,start,end,total,chunks,chunk,cuted,blob
             *
             * @param block
             */
            beforeSend: function (block) {
                console.log("beforeSend--" + block.chunk);
                $upload.text('暂停上传');
                $upload.attr('disabled',false);
                var deferred = WebUploader.Base.Deferred();
                var file = block.file;
                var type = file.ext;
                var chunk = block.chunk;
                var md5 = file.md5;
                file.chunks = block.chunks;
                console.log("webupload计算的chunks:"+block.chunks)
                /**
                 * 如果验证通过：
                 * 1：验证当前分片是否完成下载
                 */
                $.ajax({
                    url: "../../../file/getVerifyChunk",
                    data: {md5: md5, chunk: chunk},
                    type: 'POST',
                    success: function (data) {
                        console.log(data);
                        var result = JSON.parse(data);
                        var code = result.code;
                        if ("0000" == code) {
                            //验证通过
                            var isSkip = result.isSkip;
                            if (isSkip == 1) {
                                deferred.reject();
                            }
                            deferred.resolve();
                        } else {
                            console.log(result.msg);
                            deferred.resolve();
                        }
                    },
                    error: function (req, err, ex) {
                        console.log(err);
                        //执行失败
                        // deferred.fail();
                        deferred.reject();
                    }
                });
                return deferred.promise();
            },
            /**
             * 在所有分片都上传完毕后，且没有错误后request，用来做分片验证，此时如果promise被reject，当前文件上传会触发错误。
             * @param file
             */
            afterSendFile: function (file) {
                console.log("afterSendFile");
                setState("mergeing");
                var deferred = WebUploader.Base.Deferred();
                var type = file.ext;
                var md5 = file.md5;
                var chunks = file.chunks;
                /**
                 * 下载完成
                 * 合并文件
                 */
                $.ajax({
                    url: "../../../file/mergeFile",
                    data: {md5: md5, type: type, chunks: chunks},
                    type: 'POST',
                    success: function (data) {
                        console.log(data);
                        var result = JSON.parse(data);
                        var code = result.code;
                        if ("0000" == code) {
                            //验证通过
                            deferred.resolve();
                            setState("finish");
                        } else {
                            console.log(result.msg);
                            deferred.resolve();
                            uploader.retry(file);
                            $upload.text('文件重传中');
                            $upload.attr('disabled',true);
                        }
                    },
                    error: function (req, err, ex) {
                        console.log(err);
                        //执行失败
                        deferred.fail();
                        $upload.text('');
                        $upload.attr('disabled',false);
                    }
                });
            }
        });
        console.log("flashVersion:" + flashVersion);
        if (!WebUploader.Uploader.support('flash') && WebUploader.browser.ie) {

            // flash 安装了但是版本过低。
            if (flashVersion) {
                (function (container) {
                    window['expressinstallcallback'] = function (state) {
                        switch (state) {
                            case 'Download.Cancelled':
                                alert('您取消了更新！')
                                break;

                            case 'Download.Failed':
                                alert('安装失败')
                                break;

                            default:
                                alert('安装已成功，请刷新！');
                                break;
                        }
                        delete window['expressinstallcallback'];
                    };

                    var swf = './expressInstall.swf';
                    // insert flash object
                    var html = '<object type="application/' +
                        'x-shockwave-flash" data="' + swf + '" ';

                    if (WebUploader.browser.ie) {
                        html += 'classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" ';
                    }

                    html += 'width="100%" height="100%" style="outline:0">' +
                        '<param name="movie" value="' + swf + '" />' +
                        '<param name="wmode" value="transparent" />' +
                        '<param name="allowscriptaccess" value="always" />' +
                        '</object>';

                    container.html(html);

                })($wrap);

                // 压根就没有安转。
            } else {
                $wrap.html('<a href="https://www.flash.cn/" target="_blank" border="0"><img alt="get flash player" src="http://www.adobe.com/macromedia/style_guide/images/160x41_Get_Flash_Player.jpg" /></a>');
            }
            // https://www.flash.cn/cdm/latest/flashplayerax_install_cn.exe

            return;
        } else if (!WebUploader.Uploader.support()) {
            alert('Web Uploader 不支持您的浏览器！');
            return;
        }

        // 实例化
        uploader = WebUploader.create({
            pick: {
                id: '#filePicker',
                label: '点击选择文件'
            },
            formData: {
                uid: 123
            },
            // 选完文件后，是否自动上传。
            auto: false,
            dnd: '#dndArea',
            paste: '#uploader',
            swf: '../../dist/Uploader.swf',
            chunked: false,
            chunkSize: 10 * 1024 * 1024,
            chunkRetry: 2,
            server: '../../../file/upload',
            method: 'POST',
            timeout: 2*60*1000,
            threads: 3,
            // runtimeOrder: 'flash',//默认是'html5 flash'会按顺序判断执行
            sendAsBinary: true,//true:流上传 false:文件上传
            accept: {
                title: 'file',
                // extensions: 'gif,jpg,jpeg,bmp,png,pdf,zip,rar',
                extensions: '*',
                mimeTypes: '*'
            },

            // 禁掉全局的拖拽功能。这样不会出现图片拖进页面的时候，把图片打开。
            disableGlobalDnd: true,
            fileNumLimit: 1,
            // fileSizeLimit: 30*1024 * 1024 * 1024,
            fileSingleSizeLimit: 70 * 1024 * 1024 * 1024
            // duplicate: true
        });

        //选择文件之后进行排序
        uploader.on('filesQueued', function () {
            uploader.sort(function (a, b) {
                if (a.name < b.name)
                    return -1;
                if (a.name > b.name)
                    return 1;
                return 0;
            });
        });

        //选择单个文件的回调
        uploader.on('fileQueued', function (file) {
            console.log("fileQueued")
        });

        /**
         * 当开始上传流程时触发。
         */
        uploader.on('uploadStart', function (file) {
            console.log("startUpload---"+file.name)

        });

        /**
         * 当某个文件的分块在发送前触发，主要用来询问是否要添加附带参数，大文件在开起分片上传的前提下此事件可能会触发多次。
         */
        uploader.on('uploadBeforeSend', function (object, data, headers) {
            console.log("uploadBeforeSend");
            var str="";
            for (var item in object){
                str +=item+":"+object[item]+"\n";
            }
            console.log(str);

            var s="";
            for (var i in data){
                s +=item+":"+data[i]+"\n";
            }
            console.log(str);
            console.log(s);

            var md5 = object.file.md5;
            data = $.extend(data, {
                "md5": md5,
                "chunk":object.chunk,
                "chunks":object.chunks
            });
        });
        /**
         * 当某个文件上传到服务端响应后，会派送此事件来询问服务端响应是否有效。
         */
        uploader.on('uploadAccept', function (object, ret) {
            console.log("uploadAccept--"+ret)
        });
        /**
         * 上传进度回调事件，在文件上传中，多次调用此事件
         */
        uploader.on('uploadProgress',function (file, percentage) {
           console.log("uploadProgress："+percentage)
        });
        /**
         * 当文件上传出错时触发
         */
        uploader.on('uploadError',function (file, reason) {
           console.log("uploadError:"+reason);
        });
        /**
         * 当文件上传成功时触发
         */
        uploader.on('uploadSuccess',function (file, response) {
            console.log("uploadSuccess:"+response.body);
        });
        /**
         * 不管成功或者失败，文件上传完成时触发。
         */
        uploader.on('uploadComplete',function (file) {
            console.log("uploadComplete");
        });
        /**
         * 所有文件上传结束时触发
         */
        uploader.on('uploadFinished', function () {
            console.log("uploadFinished")

        });

        uploader.on('error', function (type) {
            console.log(type);
            if (type == "F_EXCEED_SIZE") {
                //文件大小超出标了
                alert("文件大小超标了");
            }else if(type == "Q_TYPE_DENIED"){
                alert("文件类型不符合");
            }else if(type == "Q_EXCEED_SIZE_LIMIT"){
                alert("文件总大小超出");
            }else if(type == "Q_EXCEED_NUM_LIMIT"){
                alert("添加的文件数量超出")
            }
        });

        // 拖拽时不接受 js, txt 文件。
        // uploader.on( 'dndAccept', function( items ) {
        //     var denied = false,
        //         len = items.length,
        //         i = 0,
        //         // 修改js类型
        //         unAllowed = 'text/plain;application/javascript ';
        //
        //     for ( ; i < len; i++ ) {
        //         // 如果在列表里面
        //         if ( ~unAllowed.indexOf( items[ i ].type ) ) {
        //             denied = true;
        //             break;
        //         }
        //     }
        //
        //     return !denied;
        // });


        /**
         * 按钮点击事件
         */
        $upload.on('click', function () {
            if ($(this).hasClass('disabled')) {
                return false;
            }
            console.log("upload---" + state);
            if (state === 'ready') {
                uploader.upload();
            } else if (state === 'paused') {
                uploader.upload();
            } else if (state === 'uploading') {
                uploader.stop(true);
                console.log("暂停上传");
            }
        });

        // 添加“添加文件”的按钮，
        // uploader.addButton({
        //     id: '#filePicker2',
        //     label: '继续添加'
        // });

        uploader.on('ready', function () {
            // window.uploader = uploader;
            //准备-开始分片
        });


        // 当有文件添加进来时执行，负责view的创建
        function addFile(file) {
            console.log("addFile");
            var $li = $('<li id="' + file.id + '">' +
                '<p class="title">' + file.name + '</p>' +
                '<p class="imgWrap"></p>' +
                '<p class="progress"><span></span></p>' +
                '</li>'),

                $btns = $('<div class="file-panel">' +
                    '<span class="cancel">删除</span>' +
                    '<span class="rotateRight">向右旋转</span>' +
                    '<span class="rotateLeft">向左旋转</span></div>').appendTo($li),
                $prgress = $li.find('p.progress span'),
                $wrap = $li.find('p.imgWrap'),
                $info = $('<p class="error"></p>'),

                showError = function (code) {
                    switch (code) {
                        case 'exceed_size':
                            text = '文件大小超出';
                            break;

                        case 'interrupt':
                            text = '上传暂停';
                            break;

                        default:
                            text = '上传失败，请重试';
                            break;
                    }

                    $info.text(text).appendTo($li);
                };

            if (file.getStatus() === 'invalid') {
                showError(file.statusText);
            } else {
                // @todo lazyload
                $wrap.text('预览中');
                uploader.makeThumb(file, function (error, src) {
                    var img;

                    if (error) {
                        $wrap.text('不能预览');
                        return;
                    }

                    if (isSupportBase64) {
                        img = $('<img src="' + src + '">');
                        $wrap.empty().append(img);
                    }
                    // } else {
                    //     $.ajax('../../../file/upload', {
                    //         method: 'POST',
                    //         data: src,
                    //         dataType: 'json'
                    //     }).done(function (response) {
                    //         if (response.result) {
                    //             img = $('<img src="' + response.result + '">');
                    //             $wrap.empty().append(img);
                    //         } else {
                    //             $wrap.text("预览出错");
                    //         }
                    //     });
                    // }
                }, thumbnailWidth, thumbnailHeight);

                percentages[file.id] = [file.size, 0];
                file.rotation = 0;
            }
            //文件状态回调
            file.on('statuschange', function (cur, prev) {
                if (prev === 'progress') {
                    $prgress.hide().width(0);
                } else if (prev === 'queued') {
                    $li.off('mouseenter mouseleave');
                    $btns.remove();
                }

                // 成功
                if (cur === 'error' || cur === 'invalid') {
                    console.log(file.statusText);
                    showError(file.statusText);
                    percentages[file.id][1] = 1;
                } else if (cur === 'interrupt') {
                    showError('interrupt');
                } else if (cur === 'queued') {
                    percentages[file.id][1] = 0;
                } else if (cur === 'progress') {
                    $info.remove();
                    $prgress.css('display', 'block');
                } else if (cur === 'complete') {
                    $li.append('<span class="success"></span>');
                }

                $li.removeClass('state-' + prev).addClass('state-' + cur);
            });

            $li.on('mouseenter', function () {
                $btns.stop().animate({height: 30});
            });

            $li.on('mouseleave', function () {
                $btns.stop().animate({height: 0});
            });

            $btns.on('click', 'span', function () {
                var index = $(this).index(),
                    deg;

                switch (index) {
                    case 0:
                        uploader.removeFile(file);
                        return;

                    case 1:
                        file.rotation += 90;
                        break;

                    case 2:
                        file.rotation -= 90;
                        break;
                }

                if (supportTransition) {
                    deg = 'rotate(' + file.rotation + 'deg)';
                    $wrap.css({
                        '-webkit-transform': deg,
                        '-mos-transform': deg,
                        '-o-transform': deg,
                        'transform': deg
                    });
                } else {
                    $wrap.css('filter', 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + (~~((file.rotation / 90) % 4 + 4) % 4) + ')');
                    // use jquery animate to rotation
                    $({
                        rotation: rotation
                    }).animate({
                        rotation: file.rotation
                    }, {
                        easing: 'linear',
                        step: function( now ) {
                            now = now * Math.PI / 180;

                            var cos = Math.cos( now ),
                                sin = Math.sin( now );

                            $wrap.css( 'filter', "progid:DXImageTransform.Microsoft.Matrix(M11=" + cos + ",M12=" + (-sin) + ",M21=" + sin + ",M22=" + cos + ",SizingMethod='auto expand')");
                        }
                    });
                }


            });

            $li.appendTo($queue);
        }

        // 负责view的销毁
        function removeFile(file) {
            var $li = $('#' + file.id);

            delete percentages[file.id];
            updateTotalProgress();
            $li.off().find('.file-panel').off().end().remove();
        }

        function updateTotalProgress() {
            var loaded = 0,
                total = 0,
                spans = $progress.children(),
                percent;

            $.each(percentages, function (k, v) {
                total += v[0];
                loaded += v[0] * v[1];
            });

            percent = total ? loaded / total : 0;


            spans.eq(0).text(Math.round(percent * 100) + '%');
            spans.eq(1).css('width', Math.round(percent * 100) + '%');
            updateStatus();
        }

        /**
         * 更新UI显示状态
         */
        function updateStatus() {
            var text = '', stats;

            if (state === 'ready') {
                text = '选中' + fileCount + '张图片，共' +
                    WebUploader.formatSize(fileSize) + '。'
            } else if (state === 'confirm') {
                stats = uploader.getStats();
                if (stats.uploadFailNum) {
                    text = '已成功上传' + stats.successNum + '张照片至XX相册，' +
                        stats.uploadFailNum + '张照片上传失败，<a class="retry" href="#">重新上传</a>失败图片或<a class="ignore" href="#">忽略</a>'
                }

            } else {
                stats = uploader.getStats();
                text = '共' + fileCount + '张（' +
                    WebUploader.formatSize(fileSize) +
                    '），已上传' + stats.successNum + '张。';

                if (stats.uploadFailNum) {
                    text += '，失败' + stats.uploadFailNum + '张';
                }
            }

            $info.html(text);
        }

        /**
         * 设置状态
         * @param val
         */
        function setState(val) {
            console.log("setState："+val);
            var file, stats;

            if (val === state) {
                return;
            }

            $upload.removeClass('state-' + state);
            $upload.addClass('state-' + val);
            state = val;

            switch (state) {
                case 'pedding':
                    $placeHolder.removeClass('element-invisible');
                    $queue.hide();
                    $statusBar.addClass('element-invisible');
                    uploader.refresh();
                    break;

                case 'ready':
                    $placeHolder.addClass('element-invisible');
                    $('#filePicker2').removeClass('element-invisible');
                    $queue.show();
                    $statusBar.removeClass('element-invisible');
                    uploader.refresh();
                    break;

                case 'uploading':
                    $('#filePicker2').addClass('element-invisible');
                    $progress.show();
                    $upload.text('暂停上传');
                    break;

                case 'paused':
                    $progress.show();
                    $upload.text('继续上传');
                    // uploader.stop(true);

                    break;

                case 'confirm':
                    $progress.hide();
                    $('#filePicker2').removeClass('element-invisible');
                    $upload.text('开始上传');

                    stats = uploader.getStats();
                    if (stats.successNum && !stats.uploadFailNum) {
                        setState('finish');
                        return;
                    }
                    break;
                case 'finish':
                    stats = uploader.getStats();
                    if (stats.successNum) {
                        $upload.text('上传完成');
                        $upload.attr('disabled',true);
                    } else {
                        // 没有成功的图片，重设
                        state = 'done';
                        location.reload();
                    }
                    break;
                case 'mergeing':
                    console.log("合并文件中...");
                    $upload.text('文件合并中');
                    $upload.attr('disabled',true);
                    break;
            }

            updateStatus();
        }

        /**
         * 更新上传进度
         */
        uploader.onUploadProgress = function (file, percentage) {
            var $li = $('#' + file.id),
                $percent = $li.find('.progress span');

            $percent.css('width', percentage * 100 + '%');
            percentages[file.id][1] = percentage;
            updateTotalProgress();
        };

        /**
         * 添加文件回调
         * @param file
         */
        uploader.onFileQueued = function (file) {
            console.log("onFileQueued");
            fileCount++;
            fileSize += file.size;

            if (fileCount === 1) {
                $placeHolder.addClass('element-invisible');
                $statusBar.show();
            }

            addFile(file);
            setState('ready');
            updateTotalProgress();
        };
        /**
         * 删除文件回调
         * @param file
         */
        uploader.onFileDequeued = function (file) {
            console.log("onFileDequeued");
            fileCount--;
            fileSize -= file.size;

            if (!fileCount) {
                setState('pedding');
            }

            removeFile(file);
            updateTotalProgress();

        };

        /**
         * 调用的方法的回调
         */
        uploader.on('all', function (type) {
            var stats;
            switch (type) {
                case 'uploadFinished':
                    setState('confirm');
                    break;

                case 'startUpload':
                    setState('uploading');
                    break;

                case 'stopUpload':
                    setState('paused');
                    break;

            }
        });

        $info.on('click', '.retry', function () {
            uploader.retry();
        });

        $info.on('click', '.ignore', function () {
            alert('todo');
        });

        $upload.addClass('state-' + state);
        updateTotalProgress();
    });

})(jQuery);